﻿using System;
using ToolBox.Security.Configuration;
using ToolBox.Security.Interfaces;
using ToolBox.Security.Services;

namespace ToolBox.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            TokenService service = new(new Config
            {
                Audience = "Mon App Console",
                Issuer = "Mon App Console",
                Signature = "q*5-GtSdxRzQ4Js%D2&2K?r@qNdS2SuBBjjmMn&-=n3AZkqkj*5X%5nnLQbfsw#?Wukds=JXj3tTP^jq?w2$vm8#4X!Fga++wf2G",
                Duration = 86400,
            });

            string token = service.CreateToken(
                new Payload
                {
                    Id = 42,
                    Email = "lykhun@gmail.com",
                    Role = "Customer",
                    BirthDate = new DateTime(1982,5,6)
                }
            );

            Console.WriteLine(token);
        }


    }

    class Payload : IPayload
    {
        public string Email { get; set; }
        public string Role { get; set; }
        public int Id { get; set; }
        public DateTime BirthDate { get; set; }
        public string Identifier 
        { 
            get { return Id.ToString(); } 
        }
    }
}
